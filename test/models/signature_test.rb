require 'test_helper'

class SignatureTest < ActiveSupport::TestCase
  test "valid is valid" do
    signature = Signature.new name: 'David', zip_code: '94117'
    assert signature.valid?
  end

  test "require a name" do
    signature = Signature.new name: '', zip_code: '94117'
    refute signature.valid?
  end

  test "require a uniqe name per zip code" do
    Signature.create name: 'David', zip_code: '94117'
    signature = Signature.new name: 'David', zip_code: '94117'
    refute signature.valid?
  end

  test "require a zip code" do
    signature = Signature.new name: 'David', zip_code: ''
    refute signature.valid?
  end

  test "require a 5 digit zip code" do
    signature = Signature.new name: 'David', zip_code: '0'
    refute signature.valid?
  end

  test "require a valid zip code" do
    signature = Signature.new name: 'David', zip_code: '00000'
    refute signature.valid?
  end

  test "locate a ZipLocation" do
    signature = Signature.new name: 'David', zip_code: '94117'
    signature.validate
    assert_equal signature.state, "California"
  end
end
