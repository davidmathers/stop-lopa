require 'test_helper'

class SignaturesControllerTest < ActionDispatch::IntegrationTest
  setup do
    $my_redis.redis.select 1
    $my_redis.flushdb
  end

  teardown do
    $my_redis.flushdb
    $my_redis.redis.select 0
  end

  test "should get new html" do
    get new_signature_url
    assert_response :success
  end

  test "should get new js" do
    get new_signature_url, xhr: true
    assert_equal "text/javascript", @response.content_type
  end

  test "should create signature html" do
    signature = Signature.new name: "David", zip_code: "94117"
    assert_difference('Signature.count') do
      post signatures_url, params: {
        signature: { name: signature.name, zip_code: signature.zip_code }
      }
    end
    assert_redirected_to signature_url(Signature.last)
  end

  test "should create signature js" do
    signature = Signature.new name: "David", zip_code: "94117"
    assert_difference('Signature.count') do
      post signatures_url, params: {
        signature: { name: signature.name, zip_code: signature.zip_code }
      }, xhr: true
    end
    assert_equal "text/javascript", @response.content_type
  end

  test "signature creation should change redis dashboard" do
    signature = Signature.new name: "David", zip_code: "94117"
    assert_difference('$my_redis.dashboard[0].to_i') do
      post signatures_url, params: {
        signature: { name: signature.name, zip_code: signature.zip_code }
      }
    end
  end

  test "should show signature" do
    signature = Signature.create name: "David", zip_code: "94117"
    get signature_url(signature)
    assert_response :success
  end

  test "should get dashboard" do
    get dashboard_url, as: :json
    assert_equal "application/json", @response.content_type
  end
end
