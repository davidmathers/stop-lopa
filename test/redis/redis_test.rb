require 'test_helper'

class RedisTest < ActiveSupport::TestCase
  setup do
    $my_redis.redis.select 1
    $my_redis.flushdb
  end

  teardown do
    $my_redis.flushdb
    $my_redis.redis.select 0
  end

  test "set state_leaderboard" do
    given_leaderboard = [[3, "California"], [2, "New York"], [1, "Texas"]]
    expected_states = ["California", "New York", "Texas"]

    $my_redis.state_leaderboard = given_leaderboard
    (signature_count, leading_states) = $my_redis.dashboard

    assert_equal "6", signature_count, "Bad signature_count"
    assert_equal expected_states, leading_states, "Bad state_leaderboard"
  end

  test "state_leaderboard_incr" do
    given_leaderboard = [[3, "California"], [2, "New York"], [1, "Texas"]]
    expected_states = ["California", "Texas", "New York"]

    $my_redis.state_leaderboard = given_leaderboard
    $my_redis.state_leaderboard_incr "California"
    $my_redis.state_leaderboard_incr "Texas"
    $my_redis.state_leaderboard_incr "Texas"
    (signature_count, leading_states) = $my_redis.dashboard

    assert_equal "9", signature_count, "Bad signature_count"
    assert_equal expected_states, leading_states, "Bad state_leaderboard"
  end
end
