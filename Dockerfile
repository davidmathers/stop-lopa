FROM ruby:2.4-alpine
EXPOSE 3000

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock ./

RUN apk add --no-cache --virtual build-deps build-base \
    && apk add --no-cache \
       nodejs \
       tzdata \
       libxml2-dev \
       libxslt-dev \
       postgresql-dev \
    && bundle install \
    && apk del build-deps

COPY . ./

#CMD bundle exec rails console
