# README

## Challenge

Congress is about to vote on the Loving Our Pets Act (LOPA) that
outlaws publishing pictures of cats on the internet.

Build a one-page campaign site to oppose this legislation. For
inspiration, check out:

    stopsesta.org <https://stopsesta.org>
    dearfcc.org <https://github.com/efforg/dear_fcc>

- You can create the site using any framework (or no framework).

- The site should have fields for name and zip code, but not state.

- The site should have a live counter, updated every two seconds, of
the number of signatures.  It should list the top three states with the
most signatures.

## Project

This app doesn't require javascript so it's a one-page site for users
with javascript and a two-page site for those without.

All files created or edited for the project are listed in touched_files.txt.

## Deployment

    $ docker-compose build
    $ docker-compose run web rails db:setup RAILS_ENV=production
    $ docker-compose run web rails redis:reset RAILS_ENV=production
    $ docker-compose run web rails assets:precompile RAILS_ENV=production

