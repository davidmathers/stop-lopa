// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

function dashboard_poll() {
  fetch('/dashboard.json')
    .then(response => response.json())
    .then(json => update_dashboard(json))
    .then(() => setTimeout(() => dashboard_poll(), 2000))
}

function update_dashboard({signature_count, leading_states}) {
  document.querySelector('#signature_count')
    .innerHTML = signature_count
  document.querySelector('#leading_states')
    .innerHTML = leading_states.map(state => `<li>${state}</li>`).join('')
}

setTimeout(() => dashboard_poll(), 2000)

