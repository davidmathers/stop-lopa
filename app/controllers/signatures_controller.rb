class SignaturesController < ApplicationController
  before_action :set_dashboard,
    unless: ->(controller) { controller.request.format.js? }

  # GET /
  def new
    @signature = Signature.new
    respond_to :html, :js
  end

  # POST /signatures
  def create
    @signature = Signature.new(signature_params)

    respond_to do |format|
      if @signature.save
        $my_redis.state_leaderboard_incr @signature.state
        format.html { redirect_to @signature }
        format.js { render :show }
      else
        format.html { render :new }
        format.js { render :new }
      end
    end
  end

  # GET /thankyou/1
  def show
    @signature = Signature.find(params[:id])
    respond_to :html
  end

  # GET /dashboard
  def dashboard
    respond_to :json
  end

  private

  def set_dashboard
    (@signature_count, @leading_states) = $my_redis.dashboard
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def signature_params
    params.require(:signature).permit(:name, :zip_code)
  end
end
