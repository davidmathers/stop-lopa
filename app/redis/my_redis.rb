class MyRedis
  def initialize(redis)
    @redis = redis
  end

  attr_reader :redis

  def dashboard
    redis.pipelined do
      redis.get 'signature_count'
      redis.zrevrange('state_leaderboard', 0, 2)
    end
  end

  def state_leaderboard=(state_leaderboard)
    signature_count = state_leaderboard.reduce(0) { |sum, pair| sum + pair[0] }
    redis.pipelined do
      redis.del('signature_count', 'state_leaderboard')
      redis.set('signature_count', signature_count)
      redis.zadd('state_leaderboard', state_leaderboard)
    end
  end

  def state_leaderboard_incr(state)
    redis.pipelined do
      redis.incr('signature_count')
      redis.zincrby('state_leaderboard', 1, state)
    end
  end

  def flushdb
    redis.flushdb
  end
end
