class Signature < ApplicationRecord
  validates :name,
    presence: true,
    uniqueness: { scope: :zip_code,
                  message: "has already been registered for that zip code" }

  validates :zip_code,
    presence: true,
    format: { with: /\A[0-9]{5}\z/, message: "must be 5 digits" }

  after_validation :locate_zip_code

  private

  def locate_zip_code
    return if errors[:zip_code].any?
    zip_location = ZipLocation.find_by(zip_code: zip_code)
    if zip_location
      self.state = zip_location.state
    else
      errors.add(:zip_code, "couldn't be located")
    end
  end
end
