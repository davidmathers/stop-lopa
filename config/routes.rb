Rails.application.routes.draw do
  post 'signatures', to: 'signatures#create', as: :signatures
  get '', to: 'signatures#new', as: :new_signature
  get 'thankyou/:id', to: 'signatures#show', as: :signature
  get 'dashboard', to: 'signatures#dashboard', as: :dashboard

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
