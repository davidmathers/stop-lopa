# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'csv'

def progress_bar(ratio)
  length = 70
  filled = (ratio * length).round
  empty = length - filled
  format("\r[%s>%s]", '=' * filled, ' ' * empty)
end

Rails.root.join('vendor', 'federalgovernmentzipcodes.us', 'zip_locations.csv').tap do |zip_locations_file|
  puts "Deleting zip_locations"
  ZipLocation.delete_all

  puts "Seeding zip_locations"
  CSV.read(zip_locations_file, headers: true).tap do |rows|
    size = rows.size
    rows.each_with_index do |row, index|
      ZipLocation.create!(zip_code: row[0], state: row[1])
      print progress_bar(index.succ.to_f / size)
    end
    puts
  end
end

