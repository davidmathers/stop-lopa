class CreateSignatures < ActiveRecord::Migration[5.1]
  def change
    create_table :signatures do |t|
      t.string :name, null: false
      t.string :zip_code, null: false
      t.string :state, null: false

      t.timestamps
    end
    add_index :signatures, [:name, :zip_code], unique: true
  end
end
