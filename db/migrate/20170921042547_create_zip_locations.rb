class CreateZipLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :zip_locations do |t|
      t.string :zip_code, null: false
      t.string :state, null: false
    end
    add_index :zip_locations, :zip_code
  end
end
