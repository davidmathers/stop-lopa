namespace :redis do
  desc "Reload redis values from SQL database"
  task reset: :environment do
    state_leaderboard = Signature.group(:state).count.to_a.map(&:reverse)

    # seed empty leaderboard with fake data
    if state_leaderboard.empty?
      state_leaderboard = [[3, "California"], [2, "New York"], [1, "Texas"]]
    end

    $my_redis.state_leaderboard = state_leaderboard
  end
end
